package assignment01;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

import java.util.Random;

/**
 * This ImageJ plugin identifies a circle in a noisy image
 * 
 * @author Samuel Hick
 * @version 2020-03-10
 */
public class Circle_Recognition implements PlugInFilter {
	
	public int setup(String arg, ImagePlus im) {
		return PlugInFilter.DOES_8G;
	}  

	public void run(ImageProcessor ip) {
		Random rand = new Random();
		final int w = ip.getWidth();
		final int h = ip.getHeight();
		
		// 1: Collect all image points with pixel values greater than zero:
		List<Point> pntlist = new ArrayList<Point>();
		for (int v = 0; v < h; v++) {
			for (int u = 0; u < w; u++) {
				int p = ip.getPixel(u, v);
				if (p > 0) {
					pntlist.add(new Point(u, v));
				}
			}
		}
		
		IJ.log("Found " + pntlist.size() + " foreground points.");
		
		// 5: try all of the following multiple times
		int Tries = 1000;
		int highscore = 0;
		int highscore_centerx = 0;
		int highscore_centery = 0;
		int highscore_radius = 0;
		ImageProcessor highscore_image = null;
		for (int trynr = 0; trynr < Tries; trynr++) {
			// 2: select 3 random points
			int pa = rand.nextInt(pntlist.size());
			int pb = 0;
			do {
				pb = rand.nextInt(pntlist.size());
			} while (pb == pa);
			int pc = 0;
			do {
				pc = rand.nextInt(pntlist.size());
			} while (pc == pa || pc == pb);
			//IJ.log("Try " + trynr + ", Selected random points number " + pa + ", "+ pb + ", "+ pc);
			
			// 3: Calculate Parameters of Circle
			double x1 = pntlist.get(pa).x;
			double x2 = pntlist.get(pb).x;
			double x3 = pntlist.get(pc).x;
			double y1 = pntlist.get(pa).y;
			double y2 = pntlist.get(pb).y;
			double y3 = pntlist.get(pc).y;
			// scource for circle code https://stackoverflow.com/questions/4103405/what-is-the-algorithm-for-finding-the-center-of-a-circle-from-three-points
		    double ax = (x1 + x2) / 2;
		    double ay = (y1 + y2) / 2;
		    double ux = (y1 - y2);
		    double uy = (x2 - x1);
		    double bx = (x2 + x3) / 2;
		    double by = (y2 + x3) / 2;
		    double vx = (y2 - y3);
		    double vy = (x3 - x2);
		    double dx = ax - bx;
		    double dy = ay - by;
		    double vu = vx * uy - vy * ux;
		    if (vu == 0)
		        continue; // Points are collinear, so no unique solution
		    double g = (dx * uy - dy * ux) / vu;
		    int centerx = (int) (bx + g * vx);
		    int centery = (int) (by + g * vy);
		    int radius = (int) java.lang.Math.sqrt((centerx-x1)*(centerx-x1)+(centery-y1)*(centery-y1));
		    
		    // Convert image to RGB so we can mark points better
		    ImageProcessor cp = ip.convertToColorProcessor();
		    cp.setColor(Color.white);
		    cp.fill();
			cp.setColor(Color.blue);
			for (Point p : pntlist) {
				cp.drawDot(p.x, p.y);
			}
		    
			cp.setColor(Color.red);
		    cp.drawOval(centerx-radius, centery-radius, radius*2, radius*2);
		    //showImage(cp, "blue=original, red=circle, green=close enough");
		    
		    // 4: Count the points that are close enough to the circle
		    // Close enough is defined by being within 1 pixel of the circle in any direction
		    int close_enough = 1;
		    int[] arr = new int[3];
		    int pnts_close_enough_cnt = 0;
		    
		    for (int i = 0; i < pntlist.size(); i++) {
		    	// get current point
		    	int currentx = pntlist.get(i).x;
				int currenty = pntlist.get(i).y;
		    	
				// search for circle
				outerloop:
		    	for (int x_offset = -1 * close_enough; x_offset <= close_enough; x_offset++) {
					for (int y_offset = -1 * close_enough; y_offset <= close_enough; y_offset++) {
						// get current pixel, if its within the bounds of the image
						if(((currentx + x_offset) < 0) || ((currenty + y_offset) < 0) || ((currentx + x_offset) > w) || ((currenty + y_offset) > h)) {
							break;
						}	
						arr = cp.getPixel(currentx + x_offset, currenty + y_offset, arr);
						// check whether current pixel is part of the circle
						if(arr[0] == 255 && arr[1] == 0 && arr[2] == 0) {
							pnts_close_enough_cnt++;
							break outerloop;
						}
						
						arr[0] = 0; arr[1] = 0; arr[2] = 0;				
					}
				}
			}
		    
		    
		    
		    // 5: keep track of best circle
		    if(pnts_close_enough_cnt > highscore) {
		    	IJ.log("Number of points close enough: " + highscore);
		    	/*IJ.log("centerx " + centerx);
			    IJ.log("centery " + centery);
			    cp.setColor(Color.red);
		    	cp.drawPixel(centerx, centery);
		    	cp.drawPixel(centerx+1, centery+1);
		    	cp.drawPixel(centerx+1, centery-1);
		    	cp.drawPixel(centerx-1, centery+1);
		    	cp.drawPixel(centerx-1, centery-1);
		    	cp.setColor(Color.green);
		    	cp.drawPixel(pntlist.get(pa).x, pntlist.get(pa).y);
		    	cp.drawPixel(pntlist.get(pb).x, pntlist.get(pb).y);
		    	cp.drawPixel(pntlist.get(pc).x, pntlist.get(pc).y);
		    	IJ.log("Coordinates p1 " + pntlist.get(pa).x + pntlist.get(pa).y);
		    	IJ.log("Coordinates p2 " + pntlist.get(pb).x + pntlist.get(pb).y);
		    	IJ.log("Coordinates p3 " + pntlist.get(pc).x + pntlist.get(pc).y);
		    	showImage(cp, "blue=original, red=circle");*/
		    	highscore = pnts_close_enough_cnt;
		    	highscore_centerx = centerx;
		    	highscore_centery = centery;
		    	highscore_radius = radius;
		    	highscore_image = cp.duplicate();
		    }
		}
		
		// 6: print best circle
	    IJ.log("centerx " + highscore_centerx);
	    IJ.log("centery " + highscore_centery);
	    IJ.log("radius " + highscore_radius);
		IJ.log("Number of points close enough: " + highscore);
    	showImage(highscore_image, "blue=original, red=circle");
		IJ.log("DONE");
	}
	
	void showImage(ImageProcessor ip, String title) {
		(new ImagePlus(title, ip)).show();
	}

}
