package assignment11;

/*******************************************************************************
 * This software is provided as a supplement to the authors' textbooks on digital
 *  image processing published by Springer-Verlag in various languages and editions.
 * Permission to use and distribute this software is granted under the BSD 2-Clause 
 * "Simplified" License (see http://opensource.org/licenses/BSD-2-Clause). 
 * Copyright (c) 2006-2016 Wilhelm Burger, Mark J. Burge. All rights reserved. 
 * Visit http://imagingbook.com for additional details.
 *******************************************************************************/

import java.awt.Color;
import java.awt.Font;
import java.awt.Shape;
import java.awt.geom.Path2D;
import java.awt.geom.QuadCurve2D;
import java.util.ArrayList;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.gui.ShapeRoi;
import ij.gui.TextRoi;
import ij.plugin.filter.PlugInFilter;
import ij.process.ByteProcessor;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import imagingbook.lib.ij.IjLogStream;
import imagingbook.pub.sift.SiftDescriptor;
import imagingbook.pub.sift.SiftDetector;
import imagingbook.pub.sift.SiftMatch;
import imagingbook.pub.sift.SiftMatcher;

/**
 * This ImageJ plugin demonstrates Basic SIFT-Based image stitching It is based
 * on the sample programs provided by Prof Burger
 * 
 * @author S. Hick
 * @version 2020/05/29
 */
public class SIFT_video_tracking_v1 implements PlugInFilter {

	static {
		IjLogStream.redirectSystem();
	}

	boolean DEBUGPRINTMONTAGES = true;
	static int Threshold_Match = 200;
	static int Threshold_Distance = 10;

	static int NumberOfMatchesToShow = 8;
	static double MatchLineCurvature = 0.25;
	static double FeatureScale = 1.0;
	static double FeatureStrokewidth = 1.0;

	static boolean ShowFeatureLabels = true;

	static Color SeparatorColor = Color.black;
	static Color DescriptorColor1 = Color.green;
	static Color DescriptorColor2 = Color.green;
	static Color MatchLineColor = Color.magenta;
	static Color LabelColor = Color.yellow;
	static Font LabelFont = new Font(Font.SANS_SERIF, Font.PLAIN, 12);

	int w = 0;
	int h = 0;

	ImagePlus imp = null;

	public int setup(String arg0, ImagePlus imp) {
		this.imp = imp;
		return DOES_8G + DOES_RGB + STACK_REQUIRED + NO_CHANGES + ROI_REQUIRED;
	}

	public void run(ImageProcessor ip) {
		if (imp.getStackSize() < 2) {
			IJ.error("Stack with at least 2 images required!");
			return;
		}

		ImageStack stack = imp.getImageStack();
		w = stack.getWidth();
		h = stack.getHeight();

		// 1. Is: open image stack and convert to grayscale. this is done outside of
		// code
		// 2. Is: select ROI. also not done in code.
		// 2.5 Get ROI

		Roi roi = imp.getRoi();
		if (roi == null)
			return;

		IJ.log("ROI is: " + roi);

		// B. Extract and show ROI
		FloatProcessor FirstImage = stack.getProcessor(1).duplicate().convertToFloatProcessor();
		FloatProcessor image_ROI = FirstImage.duplicate().convertToFloatProcessor();
		image_ROI.setRoi(roi);
		image_ROI = image_ROI.crop().convertToFloatProcessor();

//		(new ImagePlus("ROI", image_ROI)).show();

		// 3. Apply SIFT to first frame and select strongest feature in ROI
		SiftDescriptor selectedDescriptor = GetStrongestSIFTinRegion(FirstImage, roi);
		// draw descriptor
		DrawDescriptor(FirstImage, selectedDescriptor);
		// create stack for results
		ImageStack Results = new ImageStack(w, h);
		Results.addSlice(FirstImage);
//		(new ImagePlus("Results", Results)).show();

		// 4. Process all images and match SIFT feature pairs
		SiftDetector.Parameters params = new SiftDetector.Parameters();
		SiftDetector FirstDetector = new SiftDetector(FirstImage, params);
		List<SiftDescriptor> Prevmatches = FirstDetector.getSiftFeatures();

		for (int StackIndex = 2; StackIndex < 20; StackIndex++) {
			FloatProcessor thisImage = stack.getProcessor(StackIndex).duplicate().convertToFloatProcessor();

			SiftDetector thisDetector = new SiftDetector(thisImage, params);
			List<SiftDescriptor> thisDescr = thisDetector.getSiftFeatures();

			// create a matcher on the first set of features:
//			IJ.log("Prevmatches list size: " + Prevmatches.size());
			SiftMatcher sm = new SiftMatcher(Prevmatches);
			// match the second set of features:
			List<SiftMatch> matches = sm.matchDescriptors(thisDescr);
			// find matching descriptor
//			IJ.log("Match list size: " + matches.size());

			double smallestdist = Integer.MAX_VALUE;
			int smallestdistindex = 0;
			for (int i = 0; i < matches.size(); i++) {
//				double dist = matches.get(i).getDescriptor1().getDistanceL2(selectedDescriptor);

				double distx = Math.abs(matches.get(i).getDescriptor1().getX() - selectedDescriptor.getX());
				double disty = Math.abs(matches.get(i).getDescriptor1().getY() - selectedDescriptor.getY());
				double distmag = Math
						.abs(matches.get(i).getDescriptor1().getMagnitude() - selectedDescriptor.getMagnitude());
				double dist = (distx + 1) * (disty + 1) * (distmag + 1);

				if (dist < smallestdist) {
					smallestdist = dist;
					smallestdistindex = i;
//					IJ.log("New smallest Dist is: " + dist);
//					IJ.log("New smallest Dist index is: " + smallestdistindex);
//					IJ.log("Magnitudes are " + matches.get(i).getDescriptor1().getMagnitude() + " and "
//							+ selectedDescriptor.getMagnitude());
				}
			}

			// -----------------------------------------------------------------------------
			if (DEBUGPRINTMONTAGES) {
				ImageProcessor montage = new ByteProcessor(2 * w, h);
				montage.insert(stack.getProcessor(StackIndex - 1), 0, 0);
				montage.insert(stack.getProcessor(StackIndex), w, 0);
				ImagePlus montageIm = new ImagePlus(imp.getShortTitle() + "-matches", montage);

				Overlay oly = new Overlay();
				oly.add(makeStraightLine(w, 0, w, h, Color.black)); // vertical separator
				int xoffset = w;

				SiftDescriptor dA = matches.get(smallestdistindex).getDescriptor1();
				SiftDescriptor dB = matches.get(smallestdistindex).getDescriptor2();

				// draw the matched SIFT markers:

				oly.add(makeSiftMarker(selectedDescriptor, 0, 0, DescriptorColor2));
				oly.add(makeSiftMarker(dA, 0, 0, DescriptorColor1));
				oly.add(makeSiftMarker(dB, xoffset, 0, DescriptorColor2));

				// draw the connecting lines:
				oly.add(makeConnectingLine(dA, dB, xoffset, 0, MatchLineColor));

				// draw the labels:
				if (ShowFeatureLabels) {
					String label = Integer.valueOf(StackIndex).toString();
					oly.add(makeSiftLabel(dA, 0, 0, label));
					oly.add(makeSiftLabel(dB, xoffset, 0, label));
				}

				if (oly != null) {
					montageIm.setOverlay(oly);
				}
				montageIm.show();
			}
			// ---------------------------------------------------------------------------

			IJ.log("New smallest manual Dist of match nr " + StackIndex + " is: " + smallestdist);

			// exclude matches that are too bad or too distant
			if (matches.get(smallestdistindex).getDistance() < Threshold_Match && smallestdist < Threshold_Distance) {
				IJ.log("Distance of match nr " + StackIndex + " is: " + matches.get(smallestdistindex).getDistance());

				// assign corresponding descriptor
				selectedDescriptor = matches.get(smallestdistindex).getDescriptor2();
//			IJ.log("New selected descriptor is: " + selectedDescriptor);

				Prevmatches = thisDescr;

				// Draw descriptor
				DrawDescriptor(thisImage, selectedDescriptor);
			} else {
				IJ.log("Skipped match nr " + StackIndex + " Because its too bad at matchbadness "
						+ matches.get(smallestdistindex).getDistance() + " and distance " + smallestdist);
			}
			Results.addSlice(thisImage);
		}

		(new ImagePlus("Results", Results)).show();
	}

	// Draw the descriptor as an intersection of white lines going across the entire

	void DrawDescriptor(FloatProcessor image, SiftDescriptor selectedDescriptor) {
		image.setColor(Color.white);
		image.drawLine((int) selectedDescriptor.getX(), 0, (int) selectedDescriptor.getX(), image.getHeight());
		image.drawLine(0, (int) selectedDescriptor.getY(), image.getWidth(), (int) selectedDescriptor.getY());
	}

	// Get the Strongest SIFT descriptor thats within the given area of interest
	SiftDescriptor GetStrongestSIFTinRegion(FloatProcessor img, Roi roi) {
		SiftDetector.Parameters params = new SiftDetector.Parameters();
		// modify SIFT parameters here if needed
		SiftDetector SD = new SiftDetector(img, params);
		List<SiftDescriptor> matches_out = SD.getSiftFeatures();
		IJ.log("SIFT features found in first image: " + matches_out.size());

		// Get SIFT descriptors that are actually in the ROI
		List<SiftDescriptor> DescriptorsInRoi = new ArrayList<SiftDescriptor>();
		for (SiftDescriptor descriptor : matches_out) {
			if (roi.containsPoint(descriptor.getX(), descriptor.getY())) {
				DescriptorsInRoi.add(descriptor);
			}
		}

		// find strongest SIFT descriptor in ROI
		SiftDescriptor Strongest = DescriptorsInRoi.get(0);
		for (SiftDescriptor thisDescriptor : DescriptorsInRoi) {
			if (thisDescriptor.getMagnitude() > Strongest.getMagnitude()) {
				Strongest = thisDescriptor;
			}
		}

//		for (int i = 0; i < matches_out.size(); i++) {
//			if (matches_out.get(i) == Strongest) {
//				IJ.log("strongest is:" + i);
//			}
//		}

		return Strongest;
	}

	void Match1Set(ImageStack stack, int StackIndex) {

		FloatProcessor Ia = stack.getProcessor(StackIndex).convertToFloatProcessor();
		FloatProcessor Ib = stack.getProcessor(StackIndex + 1).convertToFloatProcessor();

		SiftDetector.Parameters params = new SiftDetector.Parameters();
		// modify SIFT parameters here if needed

		SiftDetector sdA = new SiftDetector(Ia, params);
		SiftDetector sdB = new SiftDetector(Ib, params);

		List<SiftDescriptor> fsA = sdA.getSiftFeatures();
		List<SiftDescriptor> fsB = sdB.getSiftFeatures();

		IJ.log("SIFT features found in image 1: " + fsA.size());
		IJ.log("SIFT features found in image 2: " + fsB.size());

		// --------------------------------------------------

		IJ.log("matching ...");
		// create a matcher on the first set of features:
		SiftMatcher sm = new SiftMatcher(fsA);
		// match the second set of features:
		List<SiftMatch> matches = sm.matchDescriptors(fsB);

		// --------------------------------------------------

		ImageProcessor montage = new ByteProcessor(2 * w, h);
		montage.insert(stack.getProcessor(StackIndex), 0, 0);
		montage.insert(stack.getProcessor(StackIndex + 1), w, 0);
		ImagePlus montageIm = new ImagePlus(imp.getShortTitle() + "-matches", montage);

		Overlay oly = new Overlay();
		oly.add(makeStraightLine(w, 0, w, h, Color.black)); // vertical separator
		int xoffset = w;

		// draw the matched SIFT markers:
		int count = 1;
		for (SiftMatch m : matches) {
			SiftDescriptor dA = m.getDescriptor1();
			SiftDescriptor dB = m.getDescriptor2();
			oly.add(makeSiftMarker(dA, 0, 0, DescriptorColor1));
			oly.add(makeSiftMarker(dB, xoffset, 0, DescriptorColor2));
			count++;
			if (count > NumberOfMatchesToShow)
				break;
		}

		// draw the connecting lines:
		count = 1;
		for (SiftMatch m : matches) {
			SiftDescriptor dA = m.getDescriptor1();
			SiftDescriptor dB = m.getDescriptor2();
			oly.add(makeConnectingLine(dA, dB, xoffset, 0, MatchLineColor));
			count++;
			if (count > NumberOfMatchesToShow)
				break;
		}

		// draw the labels:
		if (ShowFeatureLabels) {
			count = 1;
			for (SiftMatch m : matches) {
				SiftDescriptor dA = m.getDescriptor1();
				SiftDescriptor dB = m.getDescriptor2();
				String label = Integer.valueOf(count).toString();
				oly.add(makeSiftLabel(dA, 0, 0, label));
				oly.add(makeSiftLabel(dB, xoffset, 0, label));
				count++;
				if (count > NumberOfMatchesToShow)
					break;
			}
		}

		if (oly != null) {
			montageIm.setOverlay(oly);
		}
		montageIm.show();

	}

	// drawing methods -------------------------------------------------

	private ShapeRoi makeStraightLine(double x1, double y1, double x2, double y2, Color col) {
		Path2D poly = new Path2D.Double();
		poly.moveTo(x1, y1);
		poly.lineTo(x2, y2);
		ShapeRoi roi = new ShapeRoi(poly);
		roi.setStrokeWidth((float) FeatureStrokewidth);
		roi.setStrokeColor(col);
		return roi;
	}

	private ShapeRoi makeSiftMarker(SiftDescriptor d, double xo, double yo, Color col) {
		double x = d.getX() + xo;
		double y = d.getY() + yo;
		double scale = FeatureScale * d.getScale();
		double orient = d.getOrientation();
		double sin = Math.sin(orient);
		double cos = Math.cos(orient);
		Path2D poly = new Path2D.Double();
		poly.moveTo(x + (sin - cos) * scale, y - (sin + cos) * scale);
		// poly.lineTo(x, y);
		poly.lineTo(x + (sin + cos) * scale, y + (sin - cos) * scale);
		poly.lineTo(x, y);
		poly.lineTo(x - (sin - cos) * scale, y + (sin + cos) * scale);
		poly.lineTo(x - (sin + cos) * scale, y - (sin - cos) * scale);
		poly.closePath();
		ShapeRoi roi = new ShapeRoi(poly);
		roi.setStrokeWidth((float) FeatureStrokewidth);
		roi.setStrokeColor(col);
		return roi;
	}

	private ShapeRoi makeConnectingLine(SiftDescriptor f1, SiftDescriptor f2, double xo, double yo, Color col) {
		double x1 = f1.getX();
		double y1 = f1.getY();
		double x2 = f2.getX() + xo;
		double y2 = f2.getY() + yo;
		double dx = x2 - x1;
		double dy = y2 - y1;
		double ctrlx = (x1 + x2) / 2 - MatchLineCurvature * dy;
		double ctrly = (y1 + y2) / 2 + MatchLineCurvature * dx;
		Shape curve = new QuadCurve2D.Double(x1, y1, ctrlx, ctrly, x2, y2);
		ShapeRoi roi = new ShapeRoi(curve);
		roi.setStrokeWidth((float) FeatureStrokewidth);
		roi.setStrokeColor(col);
		return roi;
	}

	private TextRoi makeSiftLabel(SiftDescriptor d, double xo, double yo, String text) {
		double x = d.getX() + xo;
		double y = d.getY() + yo;
		TextRoi roi = new TextRoi((int) Math.rint(x), (int) Math.rint(y), text, LabelFont);
		roi.setStrokeColor(LabelColor);
		return roi;
	}

}
