package assignment03;

import java.awt.Color;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.ShapeRoi;
import ij.plugin.filter.PlugInFilter;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import imagingbook.pub.corners.Corner;
import imagingbook.pub.corners.HarrisCornerDetector;
import imagingbook.pub.geometry.basic.Point;
import imagingbook.pub.geometry.fitting.AffineFit2D;
import imagingbook.pub.geometry.fitting.LinearFit2D;
import imagingbook.pub.geometry.fitting.ProcrustesFit;

/**
 *
 */
public class Point_Matching implements PlugInFilter {
	
	private static int cornerSize = 2;					// size of cross-markers
	
	ImagePlus im;

    public int setup(String arg, ImagePlus im) {
    	this.im = im;
        return DOES_ALL + NO_CHANGES;
    }
    
    public void run(ImageProcessor ip) {
    	// get corners
		HarrisCornerDetector cd = new HarrisCornerDetector(ip);
		List<Corner> corners = cd.getCorners();
		// draw corners
		ColorProcessor R = ip.convertToColorProcessor();
		drawCorners(R, corners, Color.green);
		//new ImagePlus("Corners from " + im.getShortTitle(), R).show();
		
		// 2: separate detected corners into two sets
		// Dividing Line between the two images
		int DividingX = 760;
		int LeftExclusion = 120;
		int RightExclusion = 1400;
		int LowerExclusion = 730;
		int ExclusionZoneWidth = 2;
		// List for corners of each side
		List<Corner> corners_left = new ArrayList<>();
		List<Corner> corners_right = new ArrayList<>();
		for (int i = 0; i < corners.size(); i++) {
			if(corners.get(i).getY() < LowerExclusion) {
				if(corners.get(i).getX() < (DividingX - ExclusionZoneWidth) && corners.get(i).getX() > LeftExclusion) {
					corners_left.add(corners.get(i));
				}else if(corners.get(i).getX() > (DividingX + ExclusionZoneWidth) && corners.get(i).getX() < RightExclusion) {
					corners_right.add(corners.get(i));
				}
			}
		}
		// draw separated corners
		ColorProcessor Corners_Separated = ip.convertToColorProcessor();
		drawCorners(Corners_Separated, corners_left, Color.green);
		drawCorners(Corners_Separated, corners_right, Color.blue);
		//new ImagePlus("Corners from " + im.getShortTitle() + " separated into two sides", Corners_Separated).show();
		
		// 3. Iterative Closest Point Algorithm
		// 3.1 Find initial transformation
		// Trim lowest strength values from longer List
		while(corners_left.size() > corners_right.size()) {
			corners_left.remove(corners_left.size() - 1);
		}
		while(corners_left.size() < corners_right.size()) {
			corners_right.remove(corners_right.size() - 1);
		}
		IJ.log("Size of corners_left " + corners_left.size() + "Size of corners_right " + corners_right.size());
		// Calc Centroids
		Point Centroid_left = CalcCentroid(corners_left);
		Point Centroid_right = CalcCentroid(corners_right);
		double Centroids_Offset_X = Centroid_right.getX() - Centroid_left.getX();
		double Centroids_Offset_Y = Centroid_right.getY() - Centroid_left.getY();
		// create initial parameters
		RealMatrix AffineParams = MatrixUtils.createRealMatrix(new double[][]
				   {{ 1,0, Centroids_Offset_X},
					{ 0,1, Centroids_Offset_Y},
					{ 0,0, 1                 }});
		IJ.log("Initial Params " + AffineParams);
		
		// 3.4 Repeat until the error does not get any smaller
		double delta_error = Integer.MAX_VALUE;
		double prev_error = Integer.MAX_VALUE;
		List<Point> MatchedPoints = new ArrayList<>();
		while(delta_error > 0.0001) {
			// 3.2 find point matches
			MatchedPoints.clear();
			for (int cntpointsouter = 0; cntpointsouter < corners_left.size(); cntpointsouter++) {
				double smallest = Integer.MAX_VALUE;
				int smallest_index = 0;
				Point currentpoint = AffineTransformOne(AffineParams, corners_left.get(cntpointsouter));
				for(int cntpointsinner=0; cntpointsinner < corners_right.size(); cntpointsinner++) {
					double distance = Math.sqrt(Math.pow((currentpoint.getX() - corners_right.get(cntpointsinner).getX()), 2) 
											  + Math.pow((currentpoint.getY() - corners_right.get(cntpointsinner).getY()), 2));
				    if(distance < smallest) {    //additional condition here
				        smallest = distance;
				        smallest_index = cntpointsinner;
				    }
				}
				MatchedPoints.add(corners_right.get(smallest_index));
			}
			
			// 3.3 Refit
			// convert to array for fitter
			Point[] P = new Point[corners_left.size()];
			Point[] Q = new Point[corners_left.size()];
			for (int j = 0; j < corners_left.size(); j++) {
				P[j] = corners_left.get(j);
				Q[j] = MatchedPoints.get(j);
			}
			LinearFit2D fitter = new AffineFit2D(P,Q);
			AffineParams = fitter.getTransformationMatrix();
			IJ.log(String.format("RMS error: \u03B5 = %.6f", Math.sqrt(fitter.getError())));
			delta_error = prev_error - Math.sqrt(fitter.getError());
			prev_error = Math.sqrt(fitter.getError());
		}
		
		IJ.log("DONE! Params: " + AffineParams);
		
		
		// show a few point matches
		ImagePlus im = new ImagePlus("Matching points", Corners_Separated);
		Overlay oly = new Overlay();
		Path2D line = new Path2D.Double();
		//IJ.log("From corner x " + corners_left.get(0).getX() + " y " + corners_left.get(0).getY() 
		//		+ " to corner x " + MatchedPoints.get(0).getX() + " y " + MatchedPoints.get(0).getY());
		line.moveTo(corners_left.get(0).getX(), corners_left.get(0).getY());
		line.lineTo(MatchedPoints.get(0).getX(), MatchedPoints.get(0).getY());
		line.moveTo(corners_left.get(10).getX(), corners_left.get(10).getY());
		line.lineTo(MatchedPoints.get(10).getX(), MatchedPoints.get(10).getY());
		line.moveTo(corners_left.get(20).getX(), corners_left.get(20).getY());
		line.lineTo(MatchedPoints.get(20).getX(), MatchedPoints.get(20).getY());
		line.moveTo(corners_left.get(35).getX(), corners_left.get(35).getY());
		line.lineTo(MatchedPoints.get(35).getX(), MatchedPoints.get(35).getY());
		ShapeRoi roi1 = new ShapeRoi(line);
		roi1.setStrokeWidth(1.0f);
		roi1.setStrokeColor(Color.red);
		oly.add(roi1);
		im.setOverlay(oly);
		im.show();
		
    }
    
    
  //-------------------------------------------------------------------
    
    Point AffineTransformOne(RealMatrix A, Point p) {
		RealVector x = MatrixUtils.createRealVector(new double[] {p.getX(), p.getY(), 1});
		RealVector xx = A.operate(x);
		Point pp = new Point.Imp(xx.getEntry(0), xx.getEntry(1));
		return pp;
	}

	//-------------------------------------------------------------------
	
	private Point CalcCentroid(List<Corner> pnts) {
		double x = 0,y = 0;
		for (int i = 0; i < pnts.size(); i++) {
			x += pnts.get(i).getX();
			y += pnts.get(i).getY();
		}
		x /= pnts.size();
		y /= pnts.size();
		
		return Point.create(x, y);
	}
    
	//-------------------------------------------------------------------
	
	private void drawCorners(ImageProcessor ip, List<Corner> corners, Color color) {
		ip.setColor(color);
		int n = 0;
		for (Corner c: corners) {
			c.draw(ip, cornerSize);
			n = n + 1;
		}
	}
	
}
