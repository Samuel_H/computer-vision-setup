package assignment04;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import imagingbook.lib.math.Eigensolver2x2;
import imagingbook.lib.math.Matrix;

/**
 * find line that corresponds to points in image
 *
 */
public class Line_Fitting_Point implements PlugInFilter {

	public int setup(String arg, ImagePlus im) {
		return DOES_8G;
	}

	public void run(ImageProcessor ip) {
		IJ.log("Starting");

// 1. Get points from image
		int w = ip.getWidth();
		int h = ip.getHeight();
		List<Point2D.Double> pnts = new ArrayList<Point2D.Double>();
// collect all points
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				int p = ip.getPixel(x, y);
				if (p == 0) {
					pnts.add(new Point2D.Double(x, y));
				}
			}
		}

		IJ.log("Found x points: " + pnts.size());

// solve Eigenwertproblem
		double xmean = 0;
		double ymean = 0;
		double x2mean = 0;
		double y2mean = 0;
		double xymean = 0;
		for (int i = 0; i < pnts.size(); i++) {
			xmean += pnts.get(i).getX();
			ymean += pnts.get(i).getY();
			x2mean += Math.pow(pnts.get(i).getX(), 2);
			y2mean += Math.pow(pnts.get(i).getY(), 2);
			xymean += pnts.get(i).getX() * pnts.get(i).getY();
		}
		xmean /= pnts.size();
		ymean /= pnts.size();
		x2mean /= pnts.size();
		y2mean /= pnts.size();
		xymean /= pnts.size();

		IJ.log("xmean: " + xmean);
		IJ.log("ymean: " + ymean);
		IJ.log("x2mean: " + x2mean);
		IJ.log("y2mean: " + y2mean);
		IJ.log("xymean: " + xymean);

// specify a 2x2 matrix:
		double[][] Mat = { { x2mean - xmean * xmean, xymean - xmean * ymean },
				{ xymean - xmean * ymean, y2mean - ymean * ymean } };

		IJ.log("Matrix: " + Matrix.toString(Mat));

		Eigensolver2x2 solver = new Eigensolver2x2(Mat);
		double[] eigenvalues = solver.getEigenvalues();
		double[][] eigenvectors = solver.getEigenvectors();

		for (int i = 0; i < 2; i++) {
// get the eigen−pair <lambda_i, x_i>
			double lambda = eigenvalues[i];
			double[] x = eigenvectors[i];

			IJ.log("i: " + i + " lambda: " + lambda);
			IJ.log("Eigenvec: " + Matrix.toString(x));

// check if M * x_i == lambda_1 * x_i
			IJ.log("Check: M * x_i = " + Matrix.toString(Matrix.multiply(Mat, x)));
			IJ.log("Check: lambda_i * x_i = " + Matrix.toString(Matrix.multiply(lambda, x)));

			double c = -1 * x[0] * xmean - x[1] * ymean;
			IJ.log("c: " + c);

			AlgebraicLine line = new AlgebraicLine(x[0], x[1], c);
			IJ.log("A: " + line.GetA());
			IJ.log("B: " + line.GetB());
			IJ.log("C: " + line.GetC());

			ImageProcessor ipres = ip.duplicate();
			line.draw(ipres, 1);
			showImage(ipres, "line");

// Calc total error
			double error = 0;
			for (int j = 0; j < pnts.size(); j++) {
				error += Math.pow(line.distance(pnts.get(j)), 2);
			}

			IJ.log("Error: " + error + "\n");
		}

	}

	void showImage(ImageProcessor ip, String title) {
		(new ImagePlus(title, ip)).show();
	}
}