package assignment04;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * find lines that correspond to points in image
 *
 */
public class Line_Fitting_Polygon implements PlugInFilter {

	public int setup(String arg, ImagePlus im) {
		return DOES_8G;
	}

	public void run(ImageProcessor ip) {
		IJ.log("Starting");

		// 1. Get points from image
		int w = ip.getWidth();
		int h = ip.getHeight();
		List<Point2D.Double> pnts_boundary = new ArrayList<Point2D.Double>();
		// collect all points
		for (int x = 1; x < w - 1; x++) {
			for (int y = 1; y < h - 1; y++) {
				// if the pixel is black check surrounding points for white interior space.
				if (ip.getPixel(x, y) == 0) {
					findwhite: for (int i = x - 1; i <= x + 1; i++) {
						for (int j = y - 1; j <= y + 1; j++) {
							if (ip.getPixel(i, j) == 255) {
								pnts_boundary.add(new Point2D.Double(x, y));
								// break out of the findwhite loop so we only add the point once
								break findwhite;
							}
						}
					}
				}
			}
		}
		IJ.log("Found x boundary points: " + pnts_boundary.size());

		/*
		 * // copy List<Point2D.Double> pnts_boundary_copy = new
		 * ArrayList<Point2D.Double>(); for (Point2D.Double p : pnts_boundary) {
		 * pnts_boundary_copy.add(p); }
		 */
		List<List<Point2D.Double>> pnts_lines_list = new ArrayList<List<Point2D.Double>>();
		List<AlgebraicLine> lines_list = new ArrayList<AlgebraicLine>();
		Random randomGenerator;
		randomGenerator = new Random();
		// while there are unassigned points left
		while (pnts_boundary.size() != 0) {
			// 2. Start at some position and collect a few points
			// pick a random point, find all points within 5 pixels of this point, make a
			// line with them, add all points within 1 pixels of the line to the line
			int initialMaxDistance = 4;
			int LaterMaxDistance = 2;
			int MaxError = 7;
			int index = randomGenerator.nextInt(pnts_boundary.size());
			Point2D.Double indexPoint = new Point2D.Double(pnts_boundary.get(index).getX(),
					pnts_boundary.get(index).getY());
			// new list for this line
			List<Point2D.Double> pnts_new = new ArrayList<Point2D.Double>();
			// pnts_lines_list.add(new ArrayList<Point2D.Double>());
			// any point thats close enough to this point gets added to the list of this
			// line
			for (int i = pnts_boundary.size() - 1; i >= 0; i--) {
				if (indexPoint.distance(pnts_boundary.get(i)) < initialMaxDistance) {
					pnts_new.add(pnts_boundary.get(i));
				}
			}

			// IJ.log("pnts_lines_list size: " + pnts_lines_list.size());
			// IJ.log("latest pnts_lines_list size: " + (pnts_new.size()));

			// while we continue to find more points to add to this line
			int prevsize = 0;
			while (prevsize < (pnts_new.size())) {
				prevsize = pnts_new.size();
				// make line from this list
				AlgebraicLine newline = new AlgebraicLine(pnts_new);
				if (newline.GetError(pnts_new) > MaxError) {
					// error too high, try again
					continue;
				}

				// get points that are close enough to the line
				for (Point2D.Double p : pnts_boundary) {
					if (newline.distance(p) < LaterMaxDistance) {
						pnts_new.add(p);
					}
				}
			}

			// only accept lines that have 30 or more points
			if (pnts_new.size() < 30) {
				continue;
			}

			// This line is done, remove its points from the master list
			for (Point2D.Double p : pnts_new) {
				pnts_boundary.remove(p);
			}
			// add to list of lists
			pnts_lines_list.add(pnts_new);
			// add to list of lines
			lines_list.add(new AlgebraicLine(pnts_new));

			// IJ.log("X points remaining: " + pnts_boundary.size());
		}

		IJ.log("Found x Lines: " + pnts_lines_list.size());

		ImageProcessor ipres = ip.convertToColorProcessor();
		for (AlgebraicLine line : lines_list) {
			ipres.setColor(new Color(randomGenerator.nextInt(255), randomGenerator.nextInt(255),
					randomGenerator.nextInt(255)));
			line.draw(ipres, 1);

			Point2D IP = new Point2D.Double();
			for (AlgebraicLine line2 : lines_list) {
				if (line != line2) {
					IP = line.intersect(line2);
					ipres.setColor(Color.red);
					ipres.drawDot((int) IP.getX(), (int) IP.getY());
				}
			}
		}

		showImage(ipres, "result");

	}

	void showImage(ImageProcessor ip, String title) {
		(new ImagePlus(title, ip)).show();
	}
}