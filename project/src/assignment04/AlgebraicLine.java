package assignment04;

import java.awt.geom.Point2D;
import java.util.List;

import ij.process.ImageProcessor;
import imagingbook.lib.math.Eigensolver2x2;

/**
 * Algebraic line class
 */
public class AlgebraicLine {
	private double ma = 0;
	private double mb = 0;
	private double mc = 0;

	private double mthreshold = 0.000001;

	// CTor with direct parameters
	public AlgebraicLine(double a, double b, double c) {
		// if a and b are both 0, throw an exception
		if (isZero(a) && isZero(b)) {
			throw new ArithmeticException("a and b both 0");
		}
		// assign external value to internal class members
		ma = a;
		mb = b;
		mc = c;
		normalize();
	}

	// CTor for line from two points
	public AlgebraicLine(Point2D p1, Point2D p2) {
		// if both points are the same throw exception
		if (isEqual(p1, p2)) {
			throw new ArithmeticException("The two points are equal");
		}
		// calculate parameters
		ma = p2.getY() - p1.getY();
		mb = p1.getX() - p2.getX();
		mc = p1.getY() * p2.getX() - p1.getX() * p2.getY();
		// if a and b are both 0, throw an exception
		if (isZero(ma) && isZero(mb)) {
			throw new ArithmeticException("a and b both 0");
		}
		normalize();
	}

	public AlgebraicLine(List<Point2D.Double> pnts) {
		double xmean = 0;
		double ymean = 0;
		double x2mean = 0;
		double y2mean = 0;
		double xymean = 0;
		// solve Eigenwertproblem
		for (int i = 0; i < pnts.size(); i++) {
			xmean += pnts.get(i).getX();
			ymean += pnts.get(i).getY();
			x2mean += Math.pow(pnts.get(i).getX(), 2);
			y2mean += Math.pow(pnts.get(i).getY(), 2);
			xymean += pnts.get(i).getX() * pnts.get(i).getY();
		}
		xmean /= pnts.size();
		ymean /= pnts.size();
		x2mean /= pnts.size();
		y2mean /= pnts.size();
		xymean /= pnts.size();

		// IJ.log("xmean: " + xmean);
		// IJ.log("ymean: " + ymean);
		// IJ.log("x2mean: " + x2mean);
		// IJ.log("y2mean: " + y2mean);
		// IJ.log("xymean: " + xymean);

		// specify a 2x2 matrix:
		double[][] Mat = { { x2mean - xmean * xmean, xymean - xmean * ymean },
				{ xymean - xmean * ymean, y2mean - ymean * ymean } };

		// IJ.log("Matrix: " + Matrix.toString(Mat));

		Eigensolver2x2 solver = new Eigensolver2x2(Mat);
		double[] eigenvalues = solver.getEigenvalues();
		double[][] eigenvectors = solver.getEigenvectors();

		double c1 = -1 * eigenvectors[0][0] * xmean - eigenvectors[0][1] * ymean;
		double c2 = -1 * eigenvectors[1][0] * xmean - eigenvectors[1][1] * ymean;
		AlgebraicLine line1 = new AlgebraicLine(eigenvectors[0][0], eigenvectors[0][1], c1);
		AlgebraicLine line2 = new AlgebraicLine(eigenvectors[1][0], eigenvectors[1][1], c2);

		// Calc total error
		double error1 = 0;
		double error2 = 0;
		for (int j = 0; j < pnts.size(); j++) {
			error1 += Math.pow(line1.distance(pnts.get(j)), 2);
			error2 += Math.pow(line2.distance(pnts.get(j)), 2);
		}

		// IJ.log("Error: " + error + "\n");

		if (error1 < error2) {
			ma = line1.GetA();
			mb = line1.GetB();
			mc = line1.GetC();
		} else {
			ma = line2.GetA();
			mb = line2.GetB();
			mc = line2.GetC();
		}
	}

	// Get internal values
	public double GetA() {
		return ma;
	}

	public double GetB() {
		return mb;
	}

	public double GetC() {
		return mc;
	}

	// Draw line on a supplied image with specified thickness
	public void draw(ImageProcessor ip, double linewidth) {
		for (int x = 0; x < ip.getWidth(); x++) {
			for (int y = 0; y < ip.getHeight(); y++) {
				double d = ma * x + mb * y + mc;
				if (Math.abs(d) < (linewidth / 2 + mthreshold)) {
					// draw dot in foreground color of ip
					ip.drawDot(x, y);
				}
			}
		}
	}

	// Normalize the line. No parameter checking necessary because that was done
	// back in the CTor
	private void normalize() {
		double CommonScaleFactor = 1 / Math.sqrt(Math.pow(ma, 2) + Math.pow(mb, 2));
		ma *= CommonScaleFactor;
		mb *= CommonScaleFactor;
		mc *= CommonScaleFactor;
	}

	// Calculate minimum distance between a point and the line
	public double distance(Point2D p) {
		return Math.abs(ma * p.getX() + mb * p.getY() + mc);
	}

	// Get intersection point of two lines
	Point2D intersect(AlgebraicLine L2) {
		double y = (ma * L2.GetC() - mc * L2.GetA()) / (mb * L2.GetA() - ma * L2.GetB());
		double x = (mc * L2.GetB() - mb * L2.GetC()) / (mb * L2.GetA() - ma * L2.GetB());
		return new Point2D.Double(x, y);
	}

	// check wether a double value is zero
	public boolean isZero(double value) {
		return value >= -mthreshold && value <= mthreshold;
	}

	// check wether 2 double values are equal
	public boolean isEqual(double value1, double value2) {
		if (Double.compare(value1, value2) < mthreshold) {
			return false;
		} else {
			return true;
		}
	}

	// check wether 2 points are equal
	public boolean isEqual(Point2D p1, Point2D p2) {
		if (isEqual(p1.getX(), p2.getX()) && isEqual(p1.getY(), p2.getY())) {
			return true;
		} else {
			return false;
		}
	}

	// Get squared error between this line and a point cloud
	public double GetError(List<Point2D.Double> pnts) {
		double error = 0;
		for (int j = 0; j < pnts.size(); j++) {
			error += Math.pow(this.distance(pnts.get(j)), 2);
		}
		return error;
	}

}
