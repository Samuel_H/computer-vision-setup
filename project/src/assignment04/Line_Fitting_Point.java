package assignment04;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import imagingbook.lib.math.Eigensolver2x2;
import imagingbook.lib.math.Matrix;
import imagingbook.pub.threshold.global.GlobalThresholder;
import imagingbook.pub.threshold.global.OtsuThresholder;

/**
 * find line that corresponds to points in image
 *
 */
public class Line_Fitting_Point implements PlugInFilter {
	private double xmean = 0;
	private double ymean = 0;
	private double x2mean = 0;
	private double y2mean = 0;
	private double xymean = 0;
	private double[] eigenvalues = {};
	private double[][] eigenvectors = {};

	public int setup(String arg, ImagePlus im) {
		return DOES_8G;
	}

	public void run(ImageProcessor ip) {
		IJ.log("Starting");

		// 1. Get points from image
		int w = ip.getWidth();
		int h = ip.getHeight();
		List<Point2D.Double> pnts = new ArrayList<Point2D.Double>();
		// collect all points
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				int p = ip.getPixel(x, y);
				if (p == 0) {
					pnts.add(new Point2D.Double(x, y));
				}
			}
		}

		IJ.log("Found x points: " + pnts.size());

		AlgebraicLine line = FitLineToPoints(pnts);

		ImageProcessor ipres = ip.duplicate();
		line.draw(ipres, 1);
		showImage(ipres, "line");

	}

	AlgebraicLine FitLineToPoints(List<Point2D.Double> pnts) {
		// solve Eigenwertproblem
		for (int i = 0; i < pnts.size(); i++) {
			xmean += pnts.get(i).getX();
			ymean += pnts.get(i).getY();
			x2mean += Math.pow(pnts.get(i).getX(), 2);
			y2mean += Math.pow(pnts.get(i).getY(), 2);
			xymean += pnts.get(i).getX() * pnts.get(i).getY();
		}
		xmean /= pnts.size();
		ymean /= pnts.size();
		x2mean /= pnts.size();
		y2mean /= pnts.size();
		xymean /= pnts.size();

		// IJ.log("xmean: " + xmean);
		// IJ.log("ymean: " + ymean);
		// IJ.log("x2mean: " + x2mean);
		// IJ.log("y2mean: " + y2mean);
		// IJ.log("xymean: " + xymean);

		// specify a 2x2 matrix:
		double[][] Mat = { { x2mean - xmean * xmean, xymean - xmean * ymean },
				{ xymean - xmean * ymean, y2mean - ymean * ymean } };

		// IJ.log("Matrix: " + Matrix.toString(Mat));

		Eigensolver2x2 solver = new Eigensolver2x2(Mat);
		eigenvalues = solver.getEigenvalues();
		eigenvectors = solver.getEigenvectors();

		double c1 = -1 * eigenvectors[0][0] * xmean - eigenvectors[0][1] * ymean;
		double c2 = -1 * eigenvectors[1][0] * xmean - eigenvectors[1][1] * ymean;
		AlgebraicLine line1 = new AlgebraicLine(eigenvectors[0][0], eigenvectors[0][1], c1);
		AlgebraicLine line2 = new AlgebraicLine(eigenvectors[1][0], eigenvectors[1][1], c2);

		// Calc total error
		double error1 = 0;
		double error2 = 0;
		for (int j = 0; j < pnts.size(); j++) {
			error1 += Math.pow(line1.distance(pnts.get(j)), 2);
			error2 += Math.pow(line2.distance(pnts.get(j)), 2);
		}

		// IJ.log("Error: " + error + "\n");

		if (error1 < error2) {
			return line1;
		} else {
			return line2;
		}
	}

	void showImage(ImageProcessor ip, String title) {
		(new ImagePlus(title, ip)).show();
	}
}