package assignment04;

import java.awt.Color;
import java.awt.geom.Point2D;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.PlugIn;
import ij.plugin.filter.PlugInFilter;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;

/**
 *
 *
 */
public class Algebraic_Lines_Tester implements PlugIn {

	public void run(String arg0) {
		IJ.log("Starting");
		
		ImageProcessor ip = new ColorProcessor(10, 10);
		ip.setColor(Color.white);
		ip.fill();

		// line from top left to bottom right
		AlgebraicLine line1 = new AlgebraicLine(new Point2D.Double(0,0), new Point2D.Double(10,10));
		ip.setColor(Color.black);
		line1.draw(ip, 1);
		
		// line along top of screen
		AlgebraicLine line2 = new AlgebraicLine(new Point2D.Double(0,0), new Point2D.Double(10,0));
		ip.setColor(Color.blue);
		line2.draw(ip, 1);
		
		// line along bottom of screen
		AlgebraicLine line3 = new AlgebraicLine(new Point2D.Double(0,9), new Point2D.Double(9,9));
		ip.setColor(Color.green);
		line3.draw(ip, 1);
		
		// line from top right to bottom left, with smaller initial selection
		AlgebraicLine line4 = new AlgebraicLine(new Point2D.Double(5,5), new Point2D.Double(6,4));
		ip.setColor(Color.black);
		line4.draw(ip, 1);
		
		showImage(ip, "Test1");
		
		IJ.log("Point distance should be 0: " + line2.distance(new Point2D.Double(0,0)));
		IJ.log("Point distance should be 0: " + line2.distance(new Point2D.Double(0.5,0)));
		
		IJ.log("Point distance should be 1: " + line2.distance(new Point2D.Double(1,1)));
		IJ.log("Point distance should be 1.5: " + line2.distance(new Point2D.Double(1.5,1.5)));
		
		IJ.log("Intersection point should be 5,5 : " + line1.intersect(line4));
		IJ.log("Intersection point should be 5,5 : " + line4.intersect(line1));
		
		AlgebraicLine line5 = new AlgebraicLine(new Point2D.Double(0,2), new Point2D.Double(10,1));
		IJ.log("Intersection point should be 20,0 : " + line2.intersect(line5));
		IJ.log("Intersection point should be 20,0 : " + line5.intersect(line2));
	}
	
	void showImage(ImageProcessor ip, String title) {
		(new ImagePlus(title, ip)).show();
	}
}
