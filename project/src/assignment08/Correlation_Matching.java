package assignment08;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.gui.ShapeRoi;
import ij.plugin.filter.PlugInFilter;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

/**
 * This ImageJ plugin does correlation based subimage matching via three methods
 * 
 * @author Samuel Hick
 * @version 2020/05/12
 */
public class Correlation_Matching implements PlugInFilter {
	private ImagePlus imp;
	private int SearchWidth = 0;
	private int SearchHeight = 0;
	private int RoiWidth = 0;
	private int RoiHeight = 0;
	private float[][] Ia;
	private float[][] Ra;

	public int setup(String arg, ImagePlus imp) {
		this.imp = imp;
		return DOES_ALL + NO_CHANGES + ROI_REQUIRED;// (to enforce an ROI)
	}

	// --------------------------------------------------------------------

	public void run(ImageProcessor I) {
		// settings
		float ThresholdMultiplierL2 = (float) 0.22;
		float ThresholdMultiplierLCC = (float) 0.90;
		float ThresholdMultiplierNCC = (float) 0.995;

		// A. Get region of interest selected by user
		Roi roi = imp.getRoi();
		if (roi == null)
			return;

		// B. Extract and show ROI
		ImageProcessor R = I.crop();
		// (new ImagePlus("ROI", R)).show();

		// C. Convert to Floatprocessors
		FloatProcessor I_f = I.convertToFloatProcessor();
		FloatProcessor R_f = R.convertToFloatProcessor();
		Ia = I.getFloatArray();
		Ra = R.getFloatArray();

		// D. Determine size of score map D and loop vars
		SearchWidth = I.getWidth() - R.getWidth();
		SearchHeight = I.getHeight() - R.getHeight();
		RoiWidth = R.getWidth();
		RoiHeight = R.getHeight();

		// E. All three score functions
		float[][] D_L2 = new float[SearchWidth][SearchHeight];
		float[][] D_LCC = new float[SearchWidth][SearchHeight];
		float[][] D_NCC = new float[SearchWidth][SearchHeight];
		// calculate
		for (int x = 0; x < SearchWidth; x++) {
			for (int y = 0; y < SearchHeight; y++) {
				D_L2[x][y] = GetL2(R.getWidth(), R.getHeight(), x, y);
				D_LCC[x][y] = GetLCC(R.getWidth(), R.getHeight(), x, y);
				D_NCC[x][y] = GetNCC(R.getWidth(), R.getHeight(), x, y);
			}
			IJ.showProgress(x, SearchWidth);
		}

		// Show all results
		FloatProcessor D_L2_f = new FloatProcessor(D_L2);
		(new ImagePlus("D_L2_f", D_L2_f)).show();
		FloatProcessor D_LCC_f = new FloatProcessor(D_LCC);
		(new ImagePlus("D_LCC_f", D_LCC_f)).show();
		FloatProcessor D_NCC_f = new FloatProcessor(D_NCC);
		(new ImagePlus("D_NCC_f", D_NCC_f)).show();

		// Get matches and show them
		// L2
		List<int[]> matches_L2 = GetMatches(D_L2, SearchWidth, SearchHeight, true, ThresholdMultiplierL2);
		IJ.log("L2 found x matches: " + matches_L2.size());
		DrawMatches(matches_L2, I, R, "L2");
		// LCC
		List<int[]> matches_LCC = GetMatches(D_LCC, SearchWidth, SearchHeight, false, ThresholdMultiplierLCC);
		IJ.log("LCC found x matches: " + matches_LCC.size());
		DrawMatches(matches_LCC, I, R, "LCC");
		// NCC
		List<int[]> matches_NCC = GetMatches(D_NCC, SearchWidth, SearchHeight, false, ThresholdMultiplierNCC);
		IJ.log("LCC found x matches: " + matches_NCC.size());
		DrawMatches(matches_NCC, I, R, "NCC");
	}

	// -----------------------------------------------------

	void DrawMatches(List<int[]> matches, ImageProcessor I, ImageProcessor R, String name) {
		ImagePlus im = new ImagePlus("Detected matches " + name, I);
		Overlay oly = new Overlay();
		for (int[] match : matches) {
			double[] xPntsD = { match[0], match[0] + R.getWidth(), match[0] + R.getWidth(), match[0] };
			double[] yPntsD = { match[1], match[1], match[1] + R.getHeight(), match[1] + R.getHeight() };
			Path2D path = new Path2D.Double();
			path.moveTo(xPntsD[0], yPntsD[0]);
			for (int i = 1; i < xPntsD.length; i++) {
				path.lineTo(xPntsD[i], yPntsD[i]);
			}
			path.closePath();
			Roi roi2 = new ShapeRoi(path);
			roi2.setStrokeColor(Color.yellow);
			roi2.setStroke(new BasicStroke(2f)); // many more stroke options available
			oly.add(roi2);
		}
		im.setOverlay(oly);
		im.show();
	}

	// -----------------------------------------------------

	List<int[]> GetMatches(float[][] D, int width, int height, boolean IsToBeMinimized, float ThresholdMultiplier) {
		List<int[]> LikelyMatches = new ArrayList<int[]>();

		float min = GetMin2DArray(D, width, height);
		float max = GetMax2DArray(D, width, height);

		float threshold = max * ThresholdMultiplier;

		// get likely matches
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if ((IsToBeMinimized && D[x][y] < threshold) || (!IsToBeMinimized && D[x][y] > threshold)) {
					int[] res = { x, y };
					LikelyMatches.add(res);
				}
			}
		}

		// eliminate anything thats not a local Extrema
		List<int[]> Matches = new ArrayList<int[]>();
		int[] elem;
		boolean AlreadyExists;
		for (int[] likelymatch : LikelyMatches) {
			AlreadyExists = false;
			// Get local extrema near this point
			elem = GetLocalExtrema(D, LikelyMatches, likelymatch[0], likelymatch[1], RoiWidth, RoiHeight,
					IsToBeMinimized);

			// check whether this extrema is already in the list
			for (int[] match : Matches) {
				if (match[0] == elem[0] && match[1] == elem[1]) {
					AlreadyExists = true;
				}
			}

			// if it doesnt already exist add it to the list
			if (!AlreadyExists) {
				Matches.add(elem);
			}
		}

		return Matches;
	}

	// -----------------------------------------------------

	int[] GetLocalExtrema(float[][] D, List<int[]> Matches, int x, int y, int roiwidth, int roiheight,
			boolean IsToBeMinimized) {
		int[] CurrentLocalExtrema = { x, y };
		float CurrentLocalExtremaValue = Integer.MAX_VALUE;
		// if we want to find the maximum value the initial value of this must be 0
		if (!IsToBeMinimized) {
			CurrentLocalExtremaValue = 0;
		}

		for (int[] match : Matches) {
			// if this point is close enough
			if ((Math.abs(match[0] - x) < roiwidth) && (Math.abs(match[1] - y) < roiheight)) {
				// and if its smaller than the current local minimum
				if ((IsToBeMinimized && D[match[0]][match[1]] < CurrentLocalExtremaValue)
						|| (!IsToBeMinimized && D[match[0]][match[1]] > CurrentLocalExtremaValue)) {
					// make it the current local minimum
					CurrentLocalExtremaValue = D[match[0]][match[1]];
					CurrentLocalExtrema[0] = match[0];
					CurrentLocalExtrema[1] = match[1];
				}
			}
		}

		return CurrentLocalExtrema;
	}

	// -----------------------------------------------------

	float PointDistance(int x1, int y1, int x2, int y2) {
		return (float) Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
	}

	// -----------------------------------------------------

	float GetMin2DArray(float[][] D, int width, int height) {
		float res = Integer.MAX_VALUE;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (D[x][y] < res) {
					res = D[x][y];
				}
			}
		}
		return res;
	}

	// -----------------------------------------------------

	float GetMax2DArray(float[][] D, int width, int height) {
		float res = 0;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (D[x][y] > res) {
					res = D[x][y];
				}
			}
		}
		return res;
	}

	// -----------------------------------------------------

	float GetNCC(int RoiWidth, int RoiHeight, int offset_x, int offset_y) {
		float res = 0;
		float TopRes = 0;
		float LowLeftRes = 0;
		float LowRightRes = 0;
		for (int i = 0; i < RoiWidth; i++) {
			for (int j = 0; j < RoiHeight; j++) {
				float Ipoint = Ia[i + offset_x][j + offset_y];
				float Rpoint = Ra[i][j];
				TopRes += Ipoint * Rpoint;
				LowLeftRes += Math.pow(Ipoint, 2);
				LowRightRes += Math.pow(Rpoint, 2);
			}
		}
		LowLeftRes = (float) Math.sqrt(LowLeftRes);
		LowRightRes = (float) Math.sqrt(LowRightRes);
		res = TopRes / (LowRightRes * LowLeftRes);
		return res;
	}

	// -----------------------------------------------------

	float GetLCC(int RoiWidth, int RoiHeight, int offset_x, int offset_y) {
		float res = 0;
		for (int i = 0; i < RoiWidth; i++) {
			for (int j = 0; j < RoiHeight; j++) {
				float Ipoint = Ia[i + offset_x][j + offset_y];
				float Rpoint = Ra[i][j];
				res += Ipoint * Rpoint;
			}
		}
		return res;
	}

	// -----------------------------------------------------

	float GetL2(int RoiWidth, int RoiHeight, int offset_x, int offset_y) {
		float res = 0;
		for (int i = 0; i < RoiWidth; i++) {
			for (int j = 0; j < RoiHeight; j++) {
				float Ipoint = Ia[i + offset_x][j + offset_y];
				float Rpoint = Ra[i][j];
				res += Math.pow(Math.abs(Ipoint - Rpoint), 2);
			}
		}
		res = (float) Math.sqrt(res);
		return res;
	}

	// -----------------------------------------------------

	private void showAsStack(HashMap<String, FloatProcessor> eMaps) {
		ImageStack stack = new ImageStack(imp.getWidth(), imp.getHeight());

		for (String name : eMaps.keySet()) {
			stack.addSlice(name, eMaps.get(name));
		}

		ImagePlus stackIm = new ImagePlus("TextureEnergy of " + imp.getTitle(), stack);
		stackIm.setDisplayRange(0.0, 1.0);
		stackIm.show();
	}

}
