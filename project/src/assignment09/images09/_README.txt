These ZIP files contain image stacks to be opened with ImageJ. Each stack contains one pair of RGB color images.

Sources:
	http://web.cecs.pdx.edu/~fliu/project/stitch/dataset.html
	https://homes.cs.washington.edu/~shapiro/EE596/assignments/hw3/
