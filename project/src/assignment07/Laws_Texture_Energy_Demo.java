package assignment07;

import java.awt.Point;
import java.util.HashMap;

import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Roi;
import ij.io.LogStream;
import ij.plugin.filter.PlugInFilter;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

/**
 * This ImageJ plugin calculates Law's texture energy maps for the current
 * image and shows the combined results as an image stack.
 * 
 * @author W. Burger
 * @version 2020/04/21
 */
public class Laws_Texture_Energy_Demo implements PlugInFilter {
	
	static {
		LogStream.redirectSystem();		// to enable System.out.println() etc.
	}

	private ImagePlus imp;
	private int W, H;

	public int setup(String arg, ImagePlus imp) {
		this.imp = imp;
		return DOES_ALL + NO_CHANGES;	// + ROI_REQUIRED  (to enforce an ROI)
	}

	//--------------------------------------------------------------------

	public void run(ImageProcessor ip) {
		W = ip.getWidth();
		H = ip.getHeight();

		HashMap<String, FloatProcessor> eMaps = LawsTextures.makeTextureEnergyMaps(ip);
		showAsStack(eMaps);
		
		// Hint: how to obtain an array of FloatProcessors
		FloatProcessor[] emA = eMaps.values().toArray(new FloatProcessor[0]);
		
		// Hint: how to obtain the ROI and iterate over all contained points
		Roi roi = imp.getRoi();
		if (roi == null) return;
		for (Point p : roi) {
			int u = p.x;
			int v = p.y;
			// ....
		}
		
	}

	// -----------------------------------------------------

	private void showAsStack(HashMap<String, FloatProcessor> eMaps) {
		ImageStack stack = new ImageStack(W, H);
		
		for (String name : eMaps.keySet()) {
			stack.addSlice(name, eMaps.get(name));
		}

		ImagePlus stackIm = new ImagePlus("TextureEnergy of " + imp.getTitle(), stack);
		stackIm.setDisplayRange(0.0, 1.0);
		stackIm.show();
	}

}
