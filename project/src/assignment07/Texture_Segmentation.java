package assignment07;

import java.awt.Point;
import java.util.HashMap;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Roi;
import ij.plugin.filter.PlugInFilter;
import ij.process.ByteProcessor;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import imagingbook.pub.threshold.global.OtsuThresholder;

/**
 * This ImageJ plugin segments picture sections based on texture
 * 
 * @author Samuel Hick
 * @version 2020/04/24
 */
public class Texture_Segmentation implements PlugInFilter {
	private ImagePlus imp;
	private int W, H;

	public int setup(String arg, ImagePlus imp) {
		this.imp = imp;
		return DOES_ALL + NO_CHANGES + ROI_REQUIRED;// (to enforce an ROI)
	}

	// --------------------------------------------------------------------

	public void run(ImageProcessor ip) {
		W = ip.getWidth();
		H = ip.getHeight();

		// 1. Get region of interest selected by user
		Roi roi = imp.getRoi();
		if (roi == null)
			return;

		// 2. Get the 9D feature vector
		HashMap<String, FloatProcessor> eMaps = LawsTextures.makeTextureEnergyMaps(ip);
		showAsStack(eMaps);
		FloatProcessor[] emA = eMaps.values().toArray(new FloatProcessor[0]);

		// 3. Calc reference feature vector xr as the mean of all feature vectors
		float[] xr = new float[eMaps.size()];
		// counter so that we can know how many pixels are in the ROI
		int RegionPixelCnt = 0;
		// iterate over all points in the ROI and add them up
		for (Point p : roi) {
			// iterate over all 9 of laws energy maps
			for (int i = 0; i < xr.length; i++) {
				xr[i] += emA[i].getValue(p.x, p.y);
			}
			RegionPixelCnt++;
		}
		// divide by number of pnts in ROI
		for (int i = 0; i < xr.length; i++) {
			// IJ.log("xr[" + i + "]: " + xr[i] + "ROI size: " + RegionPixelCnt);
			xr[i] /= RegionPixelCnt;
			IJ.log("xr[" + i + "]: " + xr[i]);
		}

		// 4. Calc distance map D
		FloatProcessor D = (FloatProcessor) emA[0].duplicate();
		D.set(0);
		// float xr_norm = GetL1(xr);
		float xr_norm = GetL2(xr);

		float[] xc = new float[eMaps.size()];
		for (int x = 0; x < D.getWidth(); x++) {
			for (int y = 0; y < D.getHeight(); y++) {
				// fill xc vector with current values
				for (int i = 0; i < xc.length; i++) {
					xc[i] = emA[i].getf(x, y);
				}
				// get number from appropiate norm
				// float xc_norm = GetL1(xc);
				float xc_norm = GetL2(xc);
				// set number in distance map
				D.setf(x, y, Math.abs(xc_norm - xr_norm));
			}
		}

		// display D
		D.resetMinAndMax();
		(new ImagePlus("Distance map D", D)).show();

		// 5. Apply automatic otso thresholding
		OtsuThresholder otsu = new OtsuThresholder();
		ByteProcessor bytep = D.duplicate().convertToByteProcessor();
		int threshold = otsu.getThreshold(bytep);
		bytep.threshold(threshold);
		(new ImagePlus("Distance map D", bytep)).show();

	}

	// -----------------------------------------------------

	float GetL1(float[] V1) {
		float res = 0;
		for (int i = 0; i < V1.length; i++) {
			res += Math.abs(V1[i]);
		}
		return res;
	}

	// -----------------------------------------------------

	float GetL2(float[] V1) {
		float res = 0;
		for (int i = 0; i < V1.length; i++) {
			res += Math.pow(V1[i], 2);
		}
		res = (float) Math.sqrt(res);
		return res;
	}

	// -----------------------------------------------------

	private void showAsStack(HashMap<String, FloatProcessor> eMaps) {
		ImageStack stack = new ImageStack(W, H);

		for (String name : eMaps.keySet()) {
			stack.addSlice(name, eMaps.get(name));
		}

		ImagePlus stackIm = new ImagePlus("TextureEnergy of " + imp.getTitle(), stack);
		stackIm.setDisplayRange(0.0, 1.0);
		stackIm.show();
	}

}
