package assignment02;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.awt.geom.AffineTransform;


/**
 * Test for affine transformation
 * 
 * @author Samuel Hick
 *
 */
public class affine_tf implements PlugInFilter {
	
	ImagePlus im = null;
	
	public int setup(String arg, ImagePlus im) {
		this.im = im;	// keep a reference to im
		return DOES_8G + STACK_REQUIRED;
	}

	public void run(ImageProcessor ip) {
		int n = im.getStackSize();
		if (n < 2) {
			IJ.error("stack with 2 images required");
			return;
		}
		
		ImageStack stack = im.getStack();
		ImageProcessor ip_src = stack.getProcessor(1);
		
		int w = ip.getWidth();
		int h = ip.getHeight();
		List<Point2D.Double> pnts_src = new ArrayList<Point2D.Double>();

		// collect all points
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				int p = ip_src.getPixel(x, y);
				if(p>0) {
					pnts_src.add(new Point2D.Double(x, y));
				}
			}
		}
		
		// copy points to array of Point2D for Affine Transform
		Point2D srcPnts[] = new Point2D[pnts_src.size()];
		Point2D dstPnts[] = new Point2D[pnts_src.size()];
		for (int i = 0; i < srcPnts.length; i++) {
			double xx = pnts_src.get(i).x;
			double yy = pnts_src.get(i).y;
			srcPnts[i] = new Point2D.Double(xx,yy);
		}
		
		// create Transform and do transformation
		AffineTransform aff = new AffineTransform(0.013, -1.000, 1.088, -0.050, 18.688, 127.500);
		aff.transform(srcPnts, 0, dstPnts, 0, pnts_src.size());
		
		// Draw result
		ImageProcessor ip_dst = ip_src.createProcessor(w, h);
		ip_dst.setColor(Color.black);
		double residual_error = 0.0;
		for (int i = 0; i < dstPnts.length; i++) {
			double x = dstPnts[i].getX();
			double y = dstPnts[i].getY();
			int xi = (int)Math.round(x);
			int yi = (int)Math.round(y);
			ip_dst.drawDot(xi, yi);
			residual_error += dstPnts[i].distanceSq((double)xi, (double)yi);
		}
		stack.addSlice(ip_dst);
		IJ.log("Cumulative residual error: " + residual_error);
		
	}
	
	void showImage(ImageProcessor ip, String title) {
		(new ImagePlus(title, ip)).show();
	}
}
