package assignment02;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import imagingbook.pub.geometry.basic.Point;
import imagingbook.pub.geometry.delaunay.DelaunayTriangulation;
import imagingbook.pub.geometry.delaunay.Triangle;
import imagingbook.pub.geometry.delaunay.guibas.TriangulationGuibas;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularValueDecomposition;


/**
 * Triangulation
 * 
 * @author Samuel Hick
 *
 */
public class point_triangulation implements PlugInFilter {
	
	ImagePlus im = null;
	
	private double NrOfPoints = 0;
	
	// ---------------------------------------------------------------------------
	
	public int setup(String arg, ImagePlus im) {
		this.im = im;	// keep a reference to im
		return DOES_8G + STACK_REQUIRED;
	}
	
	// ---------------------------------------------------------------------------

	public void run(ImageProcessor ip) {
		int n = im.getStackSize();
		if (n < 2) {
			IJ.error("stack with 2 images required");
			return;
		}
		
		// Get image stack
		ImageStack stack = im.getStack();
		ImageProcessor ip_before = stack.getProcessor(1);
		ImageProcessor ip_after = stack.getProcessor(2);

		// Get point lists for both images
		List<Point> points_before = collectPoints(ip_before);
		List<Point> points_after = collectPoints(ip_after);
		NrOfPoints = points_before.size();
		
		// sanity check: same nr of points in both images
		if(points_before.size() != points_after.size()) {
			IJ.error("different nrs of points in both images");
			return;
		}
		
		// 1. Get Triangulations for both images --------------------------------------
		DelaunayTriangulation dt_before = new TriangulationGuibas(points_before);
		DelaunayTriangulation dt_after = new TriangulationGuibas(points_after);
		
		// sanity check: same nr of triangles
		if(dt_before.getTriangles().size() != dt_after.getTriangles().size()) {
			IJ.error("different nrs of triangles");
			return;
		}
		
		double lowesterror = Integer.MAX_VALUE;
		RealMatrix LowestErrorParams = null;
		List<Point> LowestErrorPoints = null;
		Triangle LowestErrorTriangleBefore = null;
		Triangle LowestErrorTriangleAfter = null;
		// 2. Select a triangle from each set------------------------------------------
		// for each triangle in the outer set
		for (int i = 0; i < dt_before.getTriangles().size(); i++) {
			// try each triangle in the inner set
			for (int j = 0; j < dt_before.getTriangles().size(); j++) {
				Triangle tr_before = dt_before.getTriangles().get(i);
				Triangle tr_after = dt_after.getTriangles().get(j);
				
				// 3. Get params 
				List<Point> A_1 = new ArrayList<>();
				A_1.add(tr_before.getPoints()[0]);
				A_1.add(tr_before.getPoints()[1]);
				A_1.add(tr_before.getPoints()[2]);
				
				List<Point> A_2 = new ArrayList<>();
				A_2.add(tr_before.getPoints()[1]);
				A_2.add(tr_before.getPoints()[2]);
				A_2.add(tr_before.getPoints()[0]);
				
				List<Point> A_3 = new ArrayList<>();
				A_3.add(tr_before.getPoints()[2]);
				A_3.add(tr_before.getPoints()[0]);
				A_3.add(tr_before.getPoints()[1]);
				
				List<Point> b_1 = new ArrayList<>();
				b_1.add(tr_after.getPoints()[0]);
				b_1.add(tr_after.getPoints()[1]);
				b_1.add(tr_after.getPoints()[2]);
				
				List<Point> b_2 = new ArrayList<>();
				b_2.add(tr_after.getPoints()[1]);
				b_2.add(tr_after.getPoints()[2]);
				b_2.add(tr_after.getPoints()[0]);
				
				List<Point> b_3 = new ArrayList<>();
				b_3.add(tr_after.getPoints()[2]);
				b_3.add(tr_after.getPoints()[0]);
				b_3.add(tr_after.getPoints()[1]);
				
				RealVector x_1 = GetAffineParams(A_1, b_1);
				RealVector x_2 = GetAffineParams(A_2, b_2);
				RealVector x_3 = GetAffineParams(A_3, b_3);
				
				//IJ.log("X: " + x_1.getEntry(0) + "\n"+ x_1.getEntry(1) + "\n"+ x_1.getEntry(2) + "\n"+ x_1.getEntry(3) + "\n"+ x_1.getEntry(4) + "\n"+ x_1.getEntry(5) + "\n");
				//IJ.log("X: " + x_2.getEntry(0) + "\n"+ x_2.getEntry(1) + "\n"+ x_2.getEntry(2) + "\n"+ x_2.getEntry(3) + "\n"+ x_2.getEntry(4) + "\n"+ x_2.getEntry(5) + "\n");
				//IJ.log("X: " + x_3.getEntry(0) + "\n"+ x_3.getEntry(1) + "\n"+ x_3.getEntry(2) + "\n"+ x_3.getEntry(3) + "\n"+ x_3.getEntry(4) + "\n"+ x_3.getEntry(5) + "\n");
				
				RealMatrix AffineParams_1 = MatrixUtils.createRealMatrix(new double[][]
						   {{ x_1.getEntry(0),x_1.getEntry(1), x_1.getEntry(2)},
							{ x_1.getEntry(3),x_1.getEntry(4), x_1.getEntry(5)},
							{ 0              ,0              , 1              }});
				
				RealMatrix AffineParams_2 = MatrixUtils.createRealMatrix(new double[][]
						   {{ x_2.getEntry(0),x_2.getEntry(1), x_2.getEntry(2)},
							{ x_2.getEntry(3),x_2.getEntry(4), x_2.getEntry(5)},
							{ 0              ,0              , 1              }});
				
				RealMatrix AffineParams_3 = MatrixUtils.createRealMatrix(new double[][]
						   {{ x_3.getEntry(0),x_3.getEntry(1), x_3.getEntry(2)},
							{ x_3.getEntry(3),x_3.getEntry(4), x_3.getEntry(5)},
							{ 0              ,0              , 1              }});
				
				List<Point> points_after_calc_1 = AffineTransformList(AffineParams_1, points_before);
				List<Point> points_after_calc_2 = AffineTransformList(AffineParams_2, points_before);
				List<Point> points_after_calc_3 = AffineTransformList(AffineParams_3, points_before);
				
				double error_total = CalcError(points_after, points_after_calc_1);
				if(error_total < lowesterror) {
					lowesterror = error_total;
					IJ.log("New lowest error: " + lowesterror);
					LowestErrorParams = AffineParams_1;
					LowestErrorPoints = points_after_calc_1;
					LowestErrorTriangleBefore = tr_before;
					LowestErrorTriangleAfter = tr_after;
				}
				
				error_total = CalcError(points_after, points_after_calc_2);
				if(error_total < lowesterror) {
					lowesterror = error_total;
					IJ.log("New lowest error: " + lowesterror);
					LowestErrorParams = AffineParams_2;
					LowestErrorPoints = points_after_calc_2;
					LowestErrorTriangleBefore = tr_before;
					LowestErrorTriangleAfter = tr_after;
				}
				
				error_total = CalcError(points_after, points_after_calc_3);
				if(error_total < lowesterror) {
					lowesterror = error_total;
					IJ.log("New lowest error: " + lowesterror);
					LowestErrorParams = AffineParams_3;
					LowestErrorPoints = points_after_calc_3;
					LowestErrorTriangleBefore = tr_before;
					LowestErrorTriangleAfter = tr_after;
				}
			}
		}
		
		ImageProcessor ip_result = ip_before.duplicate();
		// Least squares error
		ip_result.set(0);
		for (int i = 0; i < NrOfPoints; i++) {
			ip_result.drawPixel((int)Math.round(LowestErrorPoints.get(i).getX()), (int)Math.round(LowestErrorPoints.get(i).getY()));
		}
		
		// Do one run of least squares operation
		RealMatrix AffineParams = LeastSquares(LowestErrorParams, points_before, points_after);
		
		List<Point> points_after_calc = AffineTransformList(AffineParams, points_before);
		
		double error_total = CalcError(points_after, points_after_calc);
		IJ.log("New lowest error AFTER LEAST SQUARES: " + error_total + "\n New Params: " + AffineParams);
		
		IJ.log("DONE: params are " + AffineParams);
		ip_result.setColor(Color.black);
		for (int i = 0; i < NrOfPoints; i++) {
			ip_result.drawPixel((int)Math.round(points_after_calc.get(i).getX()), (int)Math.round(points_after_calc.get(i).getY()));
		}
		ip_result.drawLine(
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[0].getX()), 
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[0].getY()), 
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[1].getX()), 
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[1].getY()));
		ip_result.drawLine(
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[1].getX()), 
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[1].getY()), 
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[2].getX()), 
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[2].getY()));
		ip_result.drawLine(
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[2].getX()), 
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[2].getY()), 
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[0].getX()), 
				(int)Math.round(LowestErrorTriangleAfter.getPoints()[0].getY()));
		stack.addSlice(ip_result);
		
		ImageProcessor ip_input = ip_before.duplicate();
		ip_input.setColor(Color.black);
		ip_input.drawLine(
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[0].getX()), 
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[0].getY()), 
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[1].getX()), 
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[1].getY()));
		ip_input.drawLine(
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[1].getX()), 
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[1].getY()), 
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[2].getX()), 
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[2].getY()));
		ip_input.drawLine(
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[2].getX()), 
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[2].getY()), 
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[0].getX()), 
				(int)Math.round(LowestErrorTriangleBefore.getPoints()[0].getY()));
		stack.addSlice(ip_input);
		
	}
	
	// ---------------------------------------------------------------------------
	
	RealMatrix LeastSquares(RealMatrix LowestErrorParams, List<Point> points_before, List<Point> points_after){
		List<Point> pntsA = new ArrayList<>();
		List<Point> pntsb = new ArrayList<>();
		for (int cntpointsouter = 0; cntpointsouter < NrOfPoints; cntpointsouter++) {
			double smallest = Integer.MAX_VALUE;
			int smallest_cntpointsinner = 0;
			Point currentpoint = AffineTransformOne(LowestErrorParams, points_before.get(cntpointsouter));
			for(int cntpointsinner=0; cntpointsinner < NrOfPoints; cntpointsinner++) {
				double distance = Math.sqrt(Math.pow((currentpoint.getX() - points_after.get(cntpointsinner).getX()), 2) + Math.pow((currentpoint.getY() - points_after.get(cntpointsinner).getY()), 2));
			    if(distance < smallest) {    //additional condition here
			        smallest = distance;
			        smallest_cntpointsinner = cntpointsinner;
			    }
			}
			// if 2 points are close enough to each other we assume that they are the same
			pntsA.add(points_before.get(cntpointsouter));
			pntsb.add(points_after.get(smallest_cntpointsinner));
		}
		
		IJ.log("Size of pntsA " + pntsA.size() + "Size of pntsb " + pntsb.size());
		
		RealVector x = GetAffineParams(pntsA, pntsb);
		
		RealMatrix AffineParams = MatrixUtils.createRealMatrix(new double[][]
				   {{ x.getEntry(0),x.getEntry(1), x.getEntry(2)},
					{ x.getEntry(3),x.getEntry(4), x.getEntry(5)},
					{ 0            ,0            , 1            }});
		
		
		
		return AffineParams;
	}
	
	// ---------------------------------------------------------------------------
	
	double CalcError(List<Point> pnts1, List<Point> pnts2) {
		double error_accumulated = 0;
		for (int cntpointsouter = 0; cntpointsouter < NrOfPoints; cntpointsouter++) {
			double smallest = Integer.MAX_VALUE;
			Point currentpoint = pnts2.get(cntpointsouter);
			for(int cntpointsinner=0; cntpointsinner < NrOfPoints; cntpointsinner++) {
				//double distance = currentpoint.distance(pnts1.get(cntpointsinner));
				double distance = Math.sqrt(Math.pow((currentpoint.getX() - pnts1.get(cntpointsinner).getX()), 2) + Math.pow((currentpoint.getY() - pnts1.get(cntpointsinner).getY()), 2));
			    if(distance < smallest) {    //additional condition here
			        smallest = distance;
			    }
			}
			error_accumulated += Math.pow(smallest, 2);
		}
		return error_accumulated;
	}
	
	// ---------------------------------------------------------------------------
	
	List<Point> AffineTransformList(RealMatrix A, List<Point> pntsbefore){
		List<Point> pntsafter = new ArrayList<>();
		
		for (int i = 0; i < NrOfPoints; i++) {
			pntsafter.add(AffineTransformOne(A, pntsbefore.get(i)));
		}
		
		if(pntsafter.size() != NrOfPoints) {
			IJ.error("different nrs of points after TF");
		}
		
		return pntsafter;
	}
	
	// ---------------------------------------------------------------------------
	
	Point AffineTransformOne(RealMatrix A, Point p) {
		RealVector x = MatrixUtils.createRealVector(new double[] {p.getX(), p.getY(), 1});
		RealVector xx = A.operate(x);
		Point pp = new Point.Imp(xx.getEntry(0), xx.getEntry(1));
		return pp;
	}
	
	// ---------------------------------------------------------------------------
	
	RealVector GetAffineParams(List<Point> ptsA, List<Point> ptsb) {
		// enter data into matrix
		RealMatrix A = MatrixUtils.createRealMatrix(new double[ptsA.size()*2][6]);
		for (int i = 0; i < ptsA.size(); i++) {
			A.setEntry((i*2)+0, 0, ptsA.get(i).getX());
			A.setEntry((i*2)+0, 1, ptsA.get(i).getY());
			A.setEntry((i*2)+0, 2, 1);
			A.setEntry((i*2)+1, 3, ptsA.get(i).getX());
			A.setEntry((i*2)+1, 4, ptsA.get(i).getY());
			A.setEntry((i*2)+1, 5, 1);
		}
		
		// enter data into vector
		RealVector b = MatrixUtils.createRealVector(new double[ptsb.size()*2]);
		for (int i = 0; i < ptsb.size(); i++) {
			b.setEntry((i*2)+0, ptsb.get(i).getX());
			b.setEntry((i*2)+1, ptsb.get(i).getY());
		}
		
		DecompositionSolver solver = new SingularValueDecomposition(A).getSolver();
		RealVector x = solver.solve(b);
		
		return x;
	}
	
	// ---------------------------------------------------------------------------
	
	List<Point> collectPoints(ImageProcessor ip) {
		List<Point> vertices = new ArrayList<>();
		int M = ip.getWidth();
		int N = ip.getHeight();
		for (int v = 0; v < N; v++) {
			for (int u = 0; u < M; u++) {
				float val = ip.getPixelValue(u, v);
				if (val > 0) {
					vertices.add(new Point.Imp(u, v));
				}
			}
		}
		return vertices;
	}
			
	// ---------------------------------------------------------------------------
	
	void showImage(ImageProcessor ip, String title) {
		(new ImagePlus(title, ip)).show();
	}
}
