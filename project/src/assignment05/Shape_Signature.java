package assignment05;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;

/**
 * find lines that correspond to points in image
 *
 */
public class Shape_Signature implements PlugInFilter {

	public int setup(String arg, ImagePlus im) {
		return DOES_8G;
	}

	int shapecolor = 255;
	int bgcolor = 255;

	ImageProcessor ip;
	List<List<Point2D.Double>> Shapes = new ArrayList<List<Point2D.Double>>();
	List<List<Point2D.Double>> Silhouettes = new ArrayList<List<Point2D.Double>>();

	public void run(ImageProcessor ip_in) {
		IJ.log("Starting");

		boolean Debug = false;
		ip = ip_in;

		List<double[][]> ShapeSigs = new ArrayList<double[][]>();

		GetAllShapes();
		IJ.log("Found x shapes: " + Shapes.size());
		IJ.log("Found x Silhouettes: " + Silhouettes.size());
		for (int i = 0; i < Silhouettes.size(); i++) {
			IJ.log("sil x size: " + Silhouettes.get(i).size());
		}

		One_Shape_Signature oss = new One_Shape_Signature();

		for (int i = 0; i < Silhouettes.size(); i++) {
			ShapeSigs.add(oss.GetShapeSig(ip, Silhouettes.get(i), Shapes.get(i), false));
		}

		// show resulting contour histogram
		if (Debug) {
			for (int k = 0; k < ShapeSigs.size(); k++) {
				ImageProcessor ip_histo = new ColorProcessor(12, 3);
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 12; j++) {
						ip_histo.setColor((int) (2500 * ShapeSigs.get(k)[j][i]));
						ip_histo.drawPixel(j, i);
					}
				}
				Random randomGenerator = new Random();
				showImage(ip_histo, "histogram" + randomGenerator.nextInt());
			}
		}

		ColorProcessor ip_res = ip.convertToColorProcessor();
		// pick colors for first 5 silhouettes
		Color[] colors = { Color.red, Color.green, Color.blue, Color.orange, Color.cyan };
		if (Silhouettes.size() > 10) {
			// ID the 5 reference shapes
			List<Integer> RefShapesNrs = new ArrayList<>();
			for (int i = 0; i < Shapes.size(); i++) {
				if (Shapes.get(i).get(0).getY() < 135.0) {
					RefShapesNrs.add(i);
					IJ.log("Found reference shape at index: " + i);
				}
			}
			IJ.log("Found x reference shapes: " + RefShapesNrs.size());
			if (RefShapesNrs.size() != 5) {
				return;
			}

			// paint first 5 silhouettes
			for (int i = 0; i < colors.length; i++) {
				List<Point2D.Double> thisshape = Shapes.get(RefShapesNrs.get(i));
				ip_res.setColor(colors[i]);
				for (int j = 0; j < thisshape.size(); j++) {
					ip_res.drawPixel((int) Math.round(thisshape.get(j).getX()),
							(int) Math.round(thisshape.get(j).getY()));
				}
			}

			// identify closest matches
			for (int refshapenr = 0; refshapenr < RefShapesNrs.size(); refshapenr++) {
				// calc absolute differences between the signatures
				double AbsDifs[] = new double[Silhouettes.size()];
				for (int silnr = 0; silnr < Silhouettes.size(); silnr++) {
					AbsDifs[silnr] = 0;
					for (int j = 0; j < 12; j++) {
						for (int i = 0; i < 3; i++) {
							double val_this = ShapeSigs.get(refshapenr)[j][i];
							double val_other = ShapeSigs.get(silnr)[j][i];
							AbsDifs[silnr] += Math.pow(Math.abs(Math.abs(Math.abs(val_this) - Math.abs(val_other))), 2);
						}
					}
				}
				// select lowest difference as match
				int lowestindex = 0;
				double lowest = 10000;
				for (int i = 0; i < Silhouettes.size(); i++) {
					if (i == RefShapesNrs.get(refshapenr)) {
						IJ.log("skipped absdif with itself at index " + i);
						continue;
					}
					IJ.log("Absdif is: " + AbsDifs[i] + "at index " + i);
					if (AbsDifs[i] < lowest) {
						lowestindex = i;
						lowest = AbsDifs[i];
					}
				}
				IJ.log("Match is index: " + lowestindex);
				// color in shape
				ip_res.setColor(colors[refshapenr]);
				List<Point2D.Double> thisshape = Shapes.get(lowestindex);
				for (int j = 0; j < thisshape.size(); j++) {
					ip_res.drawPixel((int) Math.round(thisshape.get(j).getX()),
							(int) Math.round(thisshape.get(j).getY()));
				}

			}

		}

		showImage(ip_res, "result");

	}

	// -------------------------------------------------------------------------------------------

	private void GetAllShapes() {
		// Get points from image
		int w = ip.getWidth();
		int h = ip.getHeight();
		bgcolor = ip.getPixel(1, 1);
		IJ.log("bgcolor: " + bgcolor);

		// collect all shape points into one list
		List<Point2D.Double> allpnts = new ArrayList<Point2D.Double>();
		for (int x = 1; x < w - 1; x++) {
			for (int y = 1; y < h - 1; y++) {
				if (ip.getPixel(x, y) != bgcolor) {
					allpnts.add(new Point2D.Double(x, y));
				}
			}
		}

		IJ.log("Found x shape points: " + allpnts.size());

		// separate points into shapes
		List<Point2D.Double> thisshape;
		while (allpnts.size() > 0) {
			Shapes.add(new ArrayList<Point2D.Double>());
			thisshape = Shapes.get(Shapes.size() - 1);
			thisshape.add(allpnts.get(0));
			allpnts.remove(0);

			IJ.log("New shape, x points remain: " + allpnts.size());

			for (int j = 0; j < thisshape.size(); j++) {
				Point2D.Double thispoint = thisshape.get(j);
				// move from the back so we can delete stuff without problems
				for (int k = allpnts.size() - 1; k >= 0; k--) {
					double dist = Math.sqrt(Math.pow((thispoint.getX() - allpnts.get(k).getX()), 2)
							+ Math.pow((thispoint.getY() - allpnts.get(k).getY()), 2));
					if (dist < 3.01) {
						// close enough, add to shape list and remove from allpnts list so searching
						// gets easier
						thisshape.add(allpnts.get(k));
						allpnts.remove(k);
//						IJ.log("Remove one point, x points remain: " + allpnts.size());
					}
				}
			}
		}

		// Refine shapes into silhouettes
		for (int shapecnt = 0; shapecnt < Shapes.size(); shapecnt++) {
			thisshape = Shapes.get(shapecnt);
			Silhouettes.add(new ArrayList<Point2D.Double>());
			for (int pntcnt = 0; pntcnt < thisshape.size(); pntcnt++) {
				Point2D.Double thispoint = thisshape.get(pntcnt);
				int x = (int) Math.round(thispoint.getX());
				int y = (int) Math.round(thispoint.getY());

				// Check whether this is a silhouette point, add it if it is
				findbg: for (int i = x - 1; i <= x + 1; i++) {
					for (int j = y - 1; j <= y + 1; j++) {
						if (ip.getPixel(i, j) == bgcolor) {
							Silhouettes.get(shapecnt).add(new Point2D.Double(x, y));
							// break out of the find while loop so we only add the point once
							break findbg;
						}
					}
				}

			}

		}

		IJ.log("Found x silhouettes: " + Silhouettes.size());
	}

	// -------------------------------------------------------------------------------------------

	// check wether this point is already in one of the shapes
	private boolean IsPointKnown(Point2D.Double pnt) {
		// check all shapes
		for (int i = 0; i < Shapes.size(); i++) {
			List<Point2D.Double> thislist = Shapes.get(i);
			for (int j = 0; j < thislist.size(); j++) {
				if (thislist.get(j) == pnt) {
					return true;
				}
			}
		}
		return false;
	}

	// -------------------------------------------------------------------------------------------

	void showImage(ImageProcessor ip, String title) {
		(new ImagePlus(title, ip)).show();
	}
}