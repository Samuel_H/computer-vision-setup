package assignment05;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Line;
import ij.gui.OvalRoi;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;

/**
 * find lines that correspond to points in image
 *
 */
public class One_Shape_Signature {

	int shapecolor = 255;
	int bgcolor = 0;

	public double[][] GetShapeSig(ImageProcessor ip, List<Point2D.Double> pnts_boundary,
			List<Point2D.Double> pnts_shape, boolean Debug) {
//		IJ.log("Starting");

		Point2D.Double Center = CalcCentroid(pnts_shape);

		// 2. Find contour point pmax with greatest distance from center
		Point2D.Double pmax = pnts_boundary.get(0);
		double rmax = 0;
		for (int i = 1; i < pnts_boundary.size(); i++) {
			// double dist = pmax.distance(pnts_boundary.get(i));
			double dist = Math.sqrt(Math.pow((Center.getX() - pnts_boundary.get(i).getX()), 2)
					+ Math.pow((Center.getY() - pnts_boundary.get(i).getY()), 2));

			if (dist > rmax) {
				rmax = dist;
				pmax = pnts_boundary.get(i);
			}
		}

		if (Debug) {
			IJ.log("Furthest point is at: " + pmax);
		}

//		// draw shape and its centroid
//		if (Debug) {
//			ImageProcessor ip_boundary = ip.duplicate();
//			ip_boundary.setColor(bgcolor);
//			ip_boundary.fill();
//			ip_boundary.setColor(shapecolor);
//			for (int i = 0; i < pnts_boundary.size(); i++) {
//				ip_boundary.drawPixel((int) Math.round(pnts_boundary.get(i).getX()),
//						(int) Math.round(pnts_boundary.get(i).getY()));
//			}
//			ip_boundary.setColor(64);
//			ip_boundary.drawPixel((int) Math.round(Center.getX()), (int) Math.round(Center.getY()));
//			ip_boundary.drawPixel((int) Math.round(pmax.getX()), (int) Math.round(pmax.getY()));
//			showImage(ip_boundary, "boundary");
//		}

		// 3. Reference grid
		int NrOfCircles = 3;
		int NrOfAngularSectors = 12;
		double CircleRadius = rmax / NrOfCircles;
		// sector bins + init
		ArrayList[][] bins = new ArrayList[NrOfAngularSectors][NrOfCircles];
		for (int i = 0; i < NrOfCircles; i++) {
			for (int j = 0; j < NrOfAngularSectors; j++) {
				bins[j][i] = new ArrayList<Point2D.Double>();
			}
		}

		// sort into bins
		int angsecnr = 0;
		int circlenr = 0;
		Point2D.Double currentpnt;
		double x = pmax.getX() - Center.getX();
		double y = pmax.getY() - Center.getY();
		double pmax_angle = Math.atan2(y, x);
		if (pmax_angle < 0) {
			pmax_angle += Math.PI * 2;
		}
		if (Debug)
			IJ.log("pmax_angle:" + pmax_angle);
		double SectorAngle = Math.PI * 2 / NrOfAngularSectors;
		for (int i = 0; i < pnts_boundary.size(); i++) {
			currentpnt = CartesianToPolar(Center, pnts_boundary.get(i));
			// get circle number
			for (int j = NrOfCircles; j > 0; j--) {
				double threshold = (rmax * j) / NrOfCircles;
				if (currentpnt.getX() < threshold) {
					circlenr = j - 1;
				}
			}
			// circlenr = (int) ((rmax / NrOfCircles) / currentpnt.getX());
			// get angular sector number
//			angsecnr = (int) (((currentpnt.getY() - pmax_angle)) / SectorAngle);
////			IJ.log("angsecnr:" + angsecnr);
//
////			angsecnr = (int) ((currentpnt.getY() - pmax_angle) / SectorAngle);
//			if (angsecnr < 0) {
////				continue;
//				angsecnr += NrOfAngularSectors;
//			}

			angsecnr = -1;
			double angle = 0;
			for (int j = 0; j < NrOfAngularSectors; j++) {
				double anglebegin = j * SectorAngle + pmax_angle;
				double angleend = (j + 1) * SectorAngle + pmax_angle;
				angle = (currentpnt.getY());
				// negative angle -> convert to positive
				while (angle < 0) {
					angle += (Math.PI * 2);
				}
				if (angle < 0)
					IJ.log("Dot nr:" + i + " angle:" + angle + " still BELOW ZERO");
				// special case: both higher than a complete circle
				if (anglebegin > (Math.PI * 2) && angleend > (Math.PI * 2)) {
					anglebegin -= (Math.PI * 2);
					angleend -= (Math.PI * 2);
				}
				// special case: sector includes zero degrees
				else if (anglebegin < (Math.PI * 2) && angleend > (Math.PI * 2)) {
					if (angle >= anglebegin && angle <= angleend) {
						angsecnr = j;
						if (Debug)
							IJ.log("Dot nr:" + i + " angsecnr:" + angsecnr + " angle:" + angle + " 0deg sec");
					} else {
						angleend -= (Math.PI * 2);
						if (angle <= angleend && angle >= 0) {
							angsecnr = j;
							if (Debug)
								IJ.log("Dot nr:" + i + " angsecnr:" + angsecnr + " angle:" + angle + " 0deg sec");
						}
					}

				}

//				IJ.log("Dot nr:" + i + " x:" + pnts_boundary.get(i).getX() + " y:" + pnts_boundary.get(i).getY()
//						+ " angle:" + currentpnt.getY() + " anglebegin:" + anglebegin + " angleend:" + angleend);
				if (angle <= angleend && angle >= anglebegin) {
					angsecnr = j;
					if (Debug)
						IJ.log("Dot nr:" + i + " angsecnr:" + angsecnr + " angle:" + angle);
				}
			}
			if (angsecnr < 0) {
				IJ.log("Dot nr:" + i + " angsecnr:" + angsecnr + " angle:" + angle + " STILL FAILED");
				angsecnr = 0;
			}

			// add it to correct bin
			bins[angsecnr][circlenr].add(pnts_boundary.get(i));
		}

		if (Debug) {
			Random randomGenerator = new Random();
			Color[][] colors = new Color[NrOfAngularSectors][NrOfCircles];
			for (int i = 0; i < NrOfCircles; i++) {
				for (int j = 0; j < NrOfAngularSectors; j++) {
					colors[j][i] = new Color(randomGenerator.nextInt(255), randomGenerator.nextInt(255),
							randomGenerator.nextInt(255));
//					colors[j][i] = new Color(j, i, 127);
				}
			}

			ImageProcessor ip_bins = new ColorProcessor(1000, 1000);
			int offset = 200;
			ip_bins.setColor(Color.white);
			ip_bins.fill();
			ImagePlus im = new ImagePlus("Bins", ip_bins);

			for (int i = 0; i < NrOfCircles; i++) {
				for (int j = 0; j < NrOfAngularSectors; j++) {
//					IJ.log(bins[j][i].size() + "Elements in bin [" + j + "][" + i + "]");
					ip_bins.setColor(colors[j][i]);
					// draw dots
					for (int inbin = 0; inbin < bins[j][i].size(); inbin++) {
						Point2D thisdot = (Point2D) bins[j][i].get(inbin);
						ip_bins.drawDot((int) Math.round(thisdot.getX()) + offset,
								(int) Math.round(thisdot.getY()) + offset);
					}
					// draw overlay
					Overlay oly = new Overlay();
					for (int k = 1; k <= NrOfCircles; k++) {
						Roi circle1 = new OvalRoi(Center.getX() + offset - rmax / NrOfCircles * k,
								Center.getY() + offset - rmax / NrOfCircles * k, rmax * 2 / NrOfCircles * k,
								rmax * 2 / NrOfCircles * k);
						circle1.setStrokeColor(Color.blue);
						circle1.setStrokeWidth(0.5f);
						oly.add(circle1);
					}
					for (int k = 0; k < NrOfAngularSectors; k++) {
						Roi line = new Line(Center.getX() + offset, Center.getY() + offset,
								rmax * Math.cos(k * SectorAngle + pmax_angle) + Center.getX() + offset,
								rmax * Math.sin(k * SectorAngle + pmax_angle) + Center.getY() + offset);
						line.setStrokeColor(Color.blue);
						line.setStrokeWidth(0.5f);
						oly.add(line);
					}

					im.setOverlay(oly);

				}
			}
			ip_bins.setColor(Color.red);
			ip_bins.drawDot((int) Math.round(Center.getX() + offset), (int) Math.round(Center.getY() + offset));
			im.show();
		}

		// 4. Get bin counts
		double[][] binscnt = new double[NrOfAngularSectors][NrOfCircles];
		int total = 0;
		for (int i = 0; i < NrOfCircles; i++) {
			for (int j = 0; j < NrOfAngularSectors; j++) {
				binscnt[j][i] = bins[j][i].size();
				total += bins[j][i].size();
				if (Debug)
					IJ.log("Bin[" + j + "][" + i + "] contains:" + binscnt[j][i]);
			}
		}

		if (Debug)
			IJ.log("Total number in bins: " + total + " pnts_boundary.size(): " + pnts_boundary.size());

		// 5. normalize bin counts
		for (int i = 0; i < NrOfCircles; i++) {
			for (int j = 0; j < NrOfAngularSectors; j++) {
				binscnt[j][i] /= total;
			}
		}

		// show resulting contour histogram
		if (Debug) {
			ImageProcessor ip_histo = new ColorProcessor(NrOfAngularSectors, NrOfCircles);
			for (int i = 0; i < NrOfCircles; i++) {
				for (int j = 0; j < NrOfAngularSectors; j++) {
					ip_histo.setColor((int) (2500 * binscnt[j][i]));
					ip_histo.drawPixel(j, i);
				}
			}
			Random randomGenerator = new Random();
			showImage(ip_histo, "histogram" + randomGenerator.nextInt());
		}

		return binscnt;
	}

	// -------------------------------------------------------------------------------------------

	Point2D.Double CartesianToPolar(Point2D.Double center, Point2D.Double pnt) {
		double x = pnt.getX() - center.getX();
		double y = pnt.getY() - center.getY();

		double r = Math.sqrt(x * x + y * y);
		double theta = Math.atan2(y, x);

		return new Point2D.Double(r, theta);
	}

	// -------------------------------------------------------------------------------------------

	private Point2D.Double CalcCentroid(List<Point2D.Double> pnts) {
		double x = 0, y = 0;
		for (int i = 0; i < pnts.size(); i++) {
			x += pnts.get(i).getX();
			y += pnts.get(i).getY();
		}
		x /= pnts.size();
		y /= pnts.size();

		return new Point2D.Double(x, y);
	}

	void showImage(ImageProcessor ip, String title) {
		(new ImagePlus(title, ip)).show();
	}
}